# Modern  TV

Napsat Nette aplikaci na zobrazování dat z dazabáze, konkrétně televizní kanály a jejich skupiny

Aplikace obsahuje:

- filtrovací komponentu
- dynamicky překreslovaný snippet pomocí ajaxu
- kanály jsou grupované podle skupiny do které patří (`channelGroup`)
- kanály jsou seřazeny podle pořadí (`order`)
- Kanály ve skupině jsou seřazeny podle pořadí (`order`)
  
Každé filtrování volá pouze jeden dotaz do databáze (z databáze se vrátí již vyfiltrované hodnoty bez použití php)

BONUS 1:

Část jména co vyhledávám je zvýrazněno např.: Prima <b>Co</b>ol.

BONUS 2:

jednoduchý kanálový manažer, pro přidání, editaci a smazání

## Installation

V hlavní složce projektu nastartujeme docker

```bash
docker compose up
```

Jdeme do www složky projektu a stáhneme potřebné závislosti

```bash
cd ./www
composer install
```

vložíme do databáze potřebné záznamy ze souboru scheme.sql, například přes [adminer](http://localhost:8080/)

## Usage

Otevřeme si okno prohlížeče a [kocháme se tou nádherou](http://localhost:8081/)

## License
I don't mind