<?php

declare(strict_types=1);

namespace App\Presenters;

use App\ModernTvModule\Models\ChannelGroupModel;
use App\ModernTvModule\Models\ChannelModel;
use App\ModernTvModule\Repository\ChannelGroupRepository;
use App\ModernTvModule\Repository\Helpers\ChannelSearchCriteriaHelper;
use App\ModernTvModule\Repository\ChannelRepository;
use Nette\Application\UI\Form;


final class ChannelsPresenter extends BasePresenter
{

    /**
     * @var ChannelRepository
     */
    private ChannelRepository $channelRepository;

    /**
     * @var ChannelGroupRepository
     */
    private ChannelGroupRepository $channelGroupRepository;

    /**
     * @param ChannelRepository $channelRepository
     * @param ChannelGroupRepository $channelGroupRepository
     */
    public function __construct(
        ChannelRepository      $channelRepository,
        ChannelGroupRepository $channelGroupRepository
    )
    {
        $this->channelRepository = $channelRepository;
        $this->channelGroupRepository = $channelGroupRepository;
    }

    /**
     * @return Form
     */
    public function createComponentFilterForm(): Form
    {
        $form = new Form;
        $form->getElementPrototype()->class('ajax');

        $channelGroups = $this->channelGroupRepository->getAllGroups();
        $groups = [];
        foreach ($channelGroups as $channelGroup) {
            $groups[$channelGroup->getId()] = $channelGroup->getName();
        }

        $form->addMultiSelect('group', 'Kategorie:', $groups);
        $form->addText('name', 'Jméno');
        $form->addText('description', 'Popis');
        $form->addSubmit('send', 'Filtrovat');

        $form->onSuccess[] = [$this, 'filterFormSucceeded'];

        return $form;
    }

    /**
     * @param Form $form
     * @param $data
     * @return void
     * @throws \Nette\Application\AbortException
     */
    public function filterFormSucceeded(Form $form, $data): void
    {
        if ($this->isAjax()) {
            $searchCriteria = new ChannelSearchCriteriaHelper();
            $searchCriteria->setName($data->name);
            $searchCriteria->setDescription($data->description);
            $searchCriteria->setChannelGroups($data->group);

            $this->template->channels = $this->channelRepository->find($searchCriteria);
            $this->template->search = $searchCriteria;

            $this->redrawControl('channelListSnippet');
            $this->redrawControl('messages');
        } else {
            $this->redirect('this');
        }
    }

    /**
     * @param string $name
     * @return Form
     */
    public function createComponentCreateNewChannel(string $name): Form
    {
        $form = new Form;
        $form->getElementPrototype()->class('ajax');

        $channelGroups = $this->channelGroupRepository->getAllGroups();
        $groups = [];
        foreach ($channelGroups as $channelGroup) {
            $groups[$channelGroup->getId()] = $channelGroup->getName();
        }

        $form->addSelect('group', 'Kategorie:', $groups);
        $form->addText('name', 'Jméno');
        $form->addText('description', 'Popis');
        $form->addSubmit('send', 'Přidat');

        $form->onSuccess[] = [$this, 'createNewChannelFormSucceeded'];

        return $form;
    }

    /**
     * @param Form $form
     * @param $data
     * @return void
     * @throws \Nette\Application\AbortException
     */
    public function createNewChannelFormSucceeded(Form $form, $data): void
    {
        if ($this->isAjax()) {
            $channel = new ChannelModel();
            $channelGroup = new ChannelGroupModel();
            $channelGroup->setName($data['group']);

            $channel->setName($data['name']);
            $channel->setDescription($data['description']);
            $channel->setChannelGroup($channelGroup);

            $this->channelRepository->insert($channel);

            $form->reset();

            $channels = $this->channelRepository->getAllChannels();
            $this->template->channels = $channels;

            $this->flashMessage("Kanál přidán!", 'success');

            $this->redrawControl('addChannelForm');
            $this->redrawControl('channelListSnippet');
            $this->redrawControl('messages');
        } else {
            $this->redirect('this');
        }
    }

    /**
     * @return void
     */
    public function actionDefault()
    {
        if (!$this->isAjax()) {
            $channels = $this->channelRepository->getAllChannels();
            $this->template->channels = $channels;
        }
    }

    /**
     * @param string $id
     * @return void
     * @throws \Nette\Application\AbortException
     */
    public function handleDeleteChannel(string $id)
    {
        if ($this->isAjax()) {
            $this->channelRepository->delete($id);

            $this->flashMessage("Kanál smazán!", 'error');

            $channels = $this->channelRepository->getAllChannels();
            $this->template->channels = $channels;
            $this->redrawControl('channelListSnippet');
            $this->redrawControl('messages',);
        } else {
            $this->redirect('this');
        }
    }

    /**
     * @param string|null $id
     * @return void
     */
    public function actionEdit(?string $id)
    {

    }

    /**
     * @param string $name
     * @return Form
     */
    public function createComponentEditChannelForm(string $name): Form
    {
        $form = new Form();

        $searchCriteria = new ChannelSearchCriteriaHelper();
        $searchCriteria->setId($this->getParameter('id'));

        /** @var ChannelModel $channel */
        $channel = $this->channelRepository->find($searchCriteria)->first();

        $form->addText('id', 'Id')
            ->setDefaultValue($this->getParameter('id'))
            ->setHtmlAttribute('readonly');

        $form->addText('name', 'Jméno')
            ->setDefaultValue($channel->getName());

        $form->addText('description', 'Popis')
            ->setDefaultValue($channel->getDescription());

        $channelGroups = $this->channelGroupRepository->getAllGroups();
        $groups = [];
        foreach ($channelGroups as $channelGroup) {
            $groups[$channelGroup->getId()] = $channelGroup->getName();
        }

        $form->addSelect('group', 'Kategorie:', $groups)
            ->setDefaultValue($channel->getChannelGroup()->getId());

        $form->addSubmit('send', 'Přidat');

        $form->onSuccess[] = [$this, 'editChannelFormSucceeded'];

        return $form;
    }

    /**
     * @param Form $form
     * @param $data
     * @return void
     */
    public function editChannelFormSucceeded(Form $form, $data): void
    {
        $chanel = new ChannelModel();
        $channelGroup = new ChannelGroupModel();
        $channelGroup->setId($data['group']);

        $chanel->setId($data['id']);
        $chanel->setName($data['name']);
        $chanel->setDescription($data['description']);
        $chanel->setChannelGroup($channelGroup);

        $this->channelRepository->update($chanel);
    }

}