<?php

declare(strict_types=1);

namespace App\ModernTvModule\Models;

final class ChannelModel extends BaseModel implements ModelInterface
{
    private string $id;
    private string $name;
    private int $order;
    private string $description;

    private ChannelGroupModel $channelGroup;

    public function __construct()
    {
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder(int $order): void
    {
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return ChannelGroupModel
     */
    public function getChannelGroup(): ChannelGroupModel
    {
        return $this->channelGroup;
    }

    /**
     * @param ChannelGroupModel $channelGroup
     */
    public function setChannelGroup(ChannelGroupModel $channelGroup): void
    {
        $this->channelGroup = $channelGroup;
    }

}