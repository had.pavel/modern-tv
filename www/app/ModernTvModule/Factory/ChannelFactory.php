<?php

declare(strict_types=1);

namespace App\ModernTvModule\Factory;

use App\ModernTvModule\Models\ChannelGroupModel;
use App\ModernTvModule\Models\ChannelModel;
use App\ModernTvModule\Models\ModelInterface;
use Nette\Database\Table\ActiveRow;

final class ChannelFactory extends BaseFactory implements FactoryInterface
{

    /**
     * @inheritDoc
     */
    public static function createFromDatabaseData(ActiveRow $data): ?ModelInterface
    {
        $channel = new ChannelModel();
        $channelGroup = new ChannelGroupModel();

        $channelGroup->setId($data['groupId']);
        $channelGroup->setName($data['groupName']);
        $channelGroup->setOrder($data['groupOrder']);

        $channel->setId($data['id']);
        $channel->setName($data['name']);
        $channel->setOrder($data['order']);
        $channel->setChannelGroup($channelGroup);
        $channel->setDescription($data['description']);

        return $channel;
    }

}