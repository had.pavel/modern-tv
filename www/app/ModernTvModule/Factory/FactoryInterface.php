<?php

declare(strict_types=1);

namespace App\ModernTvModule\Factory;

use App\ModernTvModule\Models\ModelInterface;
use Nette\Database\Table\ActiveRow;

interface FactoryInterface
{

    /**
     * @param ActiveRow $data
     * @return ModelInterface|null
     */
    public static function createFromDatabaseData(ActiveRow $data): ?ModelInterface;

}