<?php

declare(strict_types=1);

namespace App\ModernTvModule\Factory;

use App\ModernTvModule\Models\ChannelGroupModel;
use App\ModernTvModule\Models\ModelInterface;
use Nette\Database\Table\ActiveRow;

final class ChannelGroupFactory extends BaseFactory implements FactoryInterface
{

    /**
     * @inheritDoc
     */
    public static function createFromDatabaseData(ActiveRow $data): ?ModelInterface
    {
        $channelGroup = new ChannelGroupModel();

        $channelGroup->setId($data['id']);
        $channelGroup->setName($data['name']);
        $channelGroup->setOrder($data['order']);

        return $channelGroup;
    }

}