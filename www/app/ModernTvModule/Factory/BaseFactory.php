<?php

declare(strict_types=1);

namespace App\ModernTvModule\Factory;

use Nette\SmartObject;

abstract class BaseFactory
{
    use SmartObject;
}