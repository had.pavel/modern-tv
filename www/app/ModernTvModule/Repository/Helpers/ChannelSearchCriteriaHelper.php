<?php

namespace App\ModernTvModule\Repository\Helpers;

class ChannelSearchCriteriaHelper
{
    private ?string $id = null;
    private ?string $name = null;
    private ?string $description = null;
    private array $channelGroups = [];

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return array
     */
    public function getChannelGroups(): array
    {
        return $this->channelGroups;
    }

    /**
     * @param array $channelGroups
     */
    public function setChannelGroups(array $channelGroups): void
    {
        $this->channelGroups = $channelGroups;
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     */
    public function setId(?string $id): void
    {
        $this->id = $id;
    }

}