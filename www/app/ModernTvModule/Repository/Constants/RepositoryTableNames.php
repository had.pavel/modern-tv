<?php

declare(strict_types=1);

namespace App\ModernTvModule\Repository\Constants;

class RepositoryTableNames
{
    const CHANNEL_TABLE_NAME = 'Channels';
    const CHANNEL_GROUP_TABLE_NAME = 'ChannelGroups';
}