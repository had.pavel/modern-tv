<?php

declare(strict_types=1);

namespace App\ModernTvModule\Repository;

use App\ModernTvModule\Factory\ChannelGroupFactory;
use App\ModernTvModule\Models\ChannelGroupModel;
use App\ModernTvModule\Repository\Constants\RepositoryTableNames;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

final class ChannelGroupRepository extends BaseRepository implements RepositoryInterface
{

    /**
     * @return Collection|ChannelGroupModel[]
     */
    public function getAllGroups(): Collection
    {
        $channelGroups = new ArrayCollection();

        $result = $this->database
            ->table(RepositoryTableNames::CHANNEL_GROUP_TABLE_NAME)
            ->select('*')
            ->order('id ASC');

        $groupsData = $result->fetchAll();

        foreach ($groupsData as $data) {
            $channelGroup = ChannelGroupFactory::createFromDatabaseData($data);
            $channelGroups->add($channelGroup);
        }

        return $channelGroups;
    }

}