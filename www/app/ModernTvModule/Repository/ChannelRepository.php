<?php

declare(strict_types=1);

namespace App\ModernTvModule\Repository;

use App\ModernTvModule\Factory\ChannelFactory;
use App\ModernTvModule\Models\ChannelModel;
use App\ModernTvModule\Repository\Constants\RepositoryTableNames;
use App\ModernTvModule\Repository\Helpers\ChannelSearchCriteriaHelper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Cocur\Slugify\Slugify;


final class ChannelRepository extends BaseRepository implements RepositoryInterface
{

    /**
     * @param string $id
     * @return int
     */
    public function delete(string $id): int
    {
        $tableName = RepositoryTableNames::CHANNEL_TABLE_NAME;

        $result = $this->database
            ->table($tableName)
            ->where("$tableName.id", $id)
            ->delete();

        return $result;
    }

    /**
     * @param ChannelModel $channel
     * @return void
     */
    public function insert(ChannelModel $channel): void
    {
        $tableName = RepositoryTableNames::CHANNEL_TABLE_NAME;
        $slugify = new Slugify();
        $order = $this->database
            ->table($tableName)
            ->max('order');

        $result = $this->database
            ->table($tableName)
            ->insert([
                'id' => $slugify->slugify($channel->getName(), '_'),
                'name' => $channel->getName(),
                'order' => $order + 1,
                'channelGroup' => $channel->getChannelGroup()->getName(),
                'description' => $channel->getDescription()
            ]);
    }

    /**
     * @return Collection|ChannelModel[]
     */
    public function find(ChannelSearchCriteriaHelper $searchCriteriaHelper): Collection
    {
        $channels = new ArrayCollection();

        $tableName = RepositoryTableNames::CHANNEL_TABLE_NAME;

        $result = $this->database
            ->table($tableName)
            ->select("
                $tableName.id, 
                $tableName.name, 
                $tableName.description, 
                $tableName.order, 
                $tableName.channelGroup,
                channelGroup.id AS groupId,
                channelGroup.name AS groupName,
                channelGroup.order AS groupOrder")
            ->order('channelGroup.order ASC')
            ->order("$tableName.order ASC");

        if (!empty($searchCriteriaHelper->getId())) {
            $result->where("$tableName.id", $searchCriteriaHelper->getId());
        }

        if (!empty($searchCriteriaHelper->getName())) {
            $result->where("$tableName.name LIKE ?", "%" . $searchCriteriaHelper->getName() . "%");
        }

        if (!empty($searchCriteriaHelper->getDescription())) {
            $result->where("$tableName.description LIKE ?", "%" . $searchCriteriaHelper->getDescription() . "%");
        }

        if (count($searchCriteriaHelper->getChannelGroups())) {
            $result->where("$tableName.channelGroup IN (?)", $searchCriteriaHelper->getChannelGroups());
        }

        $channelsData = $result->fetchAll();

        foreach ($channelsData as $data) {
            $channel = ChannelFactory::createFromDatabaseData($data);
            $channels->add($channel);
        }

        return $channels;
    }

    /**
     * @return Collection|ChannelModel[]
     */
    public function getAllChannels(): Collection
    {
        $channels = new ArrayCollection();

        $tableName = RepositoryTableNames::CHANNEL_TABLE_NAME;

        $result = $this->database
            ->table($tableName)
            ->select("
                $tableName.id, 
                $tableName.name, 
                $tableName.description, 
                $tableName.order, 
                $tableName.channelGroup,
                channelGroup.id AS groupId,
                channelGroup.name AS groupName,
                channelGroup.order AS groupOrder")
            ->order('channelGroup.order ASC')
            ->order("$tableName.order ASC");

        $channelsData = $result->fetchAll();

        foreach ($channelsData as $data) {
            $channel = ChannelFactory::createFromDatabaseData($data);
            $channels->add($channel);
        }

        return $channels;
    }

    public function update(ChannelModel $channel): void
    {
        $tableName = RepositoryTableNames::CHANNEL_TABLE_NAME;

        $result = $this->database
            ->table($tableName)
            ->where("$tableName.id", $channel->getId())
            ->update([
                'name' => $channel->getName(),
                'description' => $channel->getDescription(),
                'channelGroup' => $channel->getChannelGroup()->getId()
            ]);
    }

}